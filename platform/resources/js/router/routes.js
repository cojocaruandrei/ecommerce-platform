// const Welcome = () => import('~/pages/welcome').then(m => m.default || m)
const Login = () => import('~/pages/auth/login').then(m => m.default || m)
const Register = () => import('~/pages/auth/register').then(m => m.default || m)
const PasswordEmail = () => import('~/pages/auth/password/email').then(m => m.default || m)
const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

const Home = () => import('~/pages/home').then(m => m.default || m)

const Settings = () => import('~/pages/front/settings/index').then(m => m.default || m)
const SettingsProfile = () => import('~/pages/front/settings/profile').then(m => m.default || m)
const SettingsPassword = () => import('~/pages/front/settings/password').then(m => m.default || m)

const ProductsShow = () => import('~/pages/front/products/show').then(m => m.default || m)

const CategoriesList = () => import('~/pages/front/categories/list').then(m => m.default || m)

const Addresses = () => import('~/pages/front/addresses/index').then(m => m.default || m)
const AddressesList = () => import('~/pages/front/addresses/list').then(m => m.default || m)
const AddressesNew = () => import('~/pages/front/addresses/new').then(m => m.default || m)
const AddressesEdit = () => import('~/pages/front/addresses/edit').then(m => m.default || m)

const CartShow = () => import('~/pages/front/cart/show').then(m => m.default || m)
const CartCheckout = () => import('~/pages/front/cart/checkout').then(m => m.default || m)


export default [
    {path: '/', name: 'home', component: Home},

    {path: '/login', name: 'login', component: Login},
    {path: '/register', name: 'register', component: Register},
    {path: '/password/reset', name: 'password.request', component: PasswordEmail},
    {path: '/password/reset/:token', name: 'password.reset', component: PasswordReset},

    // { path: '/home', name: 'home', component: Home },
    {
        path: '/settings',
        component: Settings,
        children: [
            {path: '', redirect: {name: 'settings.profile'}},
            {path: 'profile', name: 'settings.profile', component: SettingsProfile},
            {path: 'password', name: 'settings.password', component: SettingsPassword}
        ]
    },

    {
        path: '/addresses',
        component: Addresses,
        children: [
            {path: '', redirect: {name: 'addresses.list'}},
            {path: 'list', name: 'addresses.list', component: AddressesList},
            {path: 'new', name: 'addresses.new', component: AddressesNew},
            {path: 'edit/:id', name: 'addresses.edit', component: AddressesEdit}
        ]
    },

    {
        path: '/products/:id', component: ProductsShow, name: 'products.show'
    },

    {
        path: '/cat/:id', component: CategoriesList, name: 'categories.list'
    },

    {path: '/cart', name: 'cart.show', component: CartShow},

    {path: '/checkout', name: 'cart.checkout', component: CartCheckout},

    {path: '*', component: NotFound}
]
