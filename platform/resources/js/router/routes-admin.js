const Admin = () => import('~/pages/admin/index').then(m => m.default || m)

const AdminDashboard = () => import('~/pages/admin/tabs/dashboard').then(m => m.default || m)

const AdminCustomers = () => import('~/pages/admin/tabs/sell/Customers/list').then(m => m.default || m)

const AdminAddresses = () => import('~/pages/admin/tabs/sell/Addresses/layout').then(m => m.default || m)
const AdminAddressesList = () => import('~/pages/admin/tabs/sell/Addresses/list').then(m => m.default || m)
const AdminAddressesNew = () => import('~/pages/admin/tabs/sell/Addresses/new').then(m => m.default || m)

const AdminProducts = () => import('~/pages/admin/tabs/sell/Products/layout').then(m => m.default || m)
const AdminProductsList = () => import('~/pages/admin/tabs/sell/Products/list').then(m => m.default || m)
const AdminProductsNew = () => import('~/pages/admin/tabs/sell/Products/new').then(m => m.default || m)

const AdminCategories = () => import('~/pages/admin/tabs/sell/Categories/layout').then(m => m.default || m)
const AdminCategoriesList = () => import('~/pages/admin/tabs/sell/Categories/list').then(m => m.default || m)
const AdminCategoriesNew = () => import('~/pages/admin/tabs/sell/Categories/new').then(m => m.default || m)

const AdminDesign = () => import('~/pages/admin/tabs/improve/design').then(m => m.default || m)

const AdminSettings = () => import('~/pages/admin/tabs/configure/settings').then(m => m.default || m)

const AdminCarriers = () => import('~/pages/admin/tabs/configure/Carriers/layout').then(m => m.default || m)
const AdminCarriersList = () => import('~/pages/admin/tabs/configure/Carriers/list').then(m => m.default || m)
const AdminCarriersNew = () => import('~/pages/admin/tabs/configure/Carriers/new').then(m => m.default || m)

export default [
    //Admin routes
    {
        path: '/admin',
        component: Admin,
        children: [
            {path: '', redirect: {name: 'admin.dashboard'}},

            {path: 'dashboard', name: 'admin.dashboard', component: AdminDashboard},

            {path: 'customers', name: 'admin.customers', component: AdminCustomers},
            {
                path: 'addresses',
                component: AdminAddresses,
                children: [
                    {path: '', redirect: {name: 'admin.addresses.list'}},
                    {path: 'list', name: 'admin.addresses.list', component: AdminAddressesList},
                    {path: 'new', name: 'admin.addresses.new', component: AdminAddressesNew},
                ]
            },
            {
                path: 'products',
                component: AdminProducts,
                children: [
                    {path: '', redirect: {name: 'admin.products.list'}},
                    {path: 'list', name: 'admin.products.list', component: AdminProductsList},
                    {path: 'new', name: 'admin.products.new', component: AdminProductsNew},
                ]
            },
            {
                path: 'categories',
                component: AdminCategories,
                children: [
                    {path: '', redirect: {name: 'admin.categories.list'}},
                    {path: 'list', name: 'admin.categories.list', component: AdminCategoriesList},
                    {path: 'new', name: 'admin.categories.new', component: AdminCategoriesNew},
                ]
            },

            {path: 'design', name: 'admin.design', component: AdminDesign},

            {path: 'settings', name: 'admin.settings', component: AdminSettings},
            {
                path: 'carriers',
                component: AdminCarriers,
                children: [
                    {path: '', redirect: {name: 'admin.carriers.list'}},
                    {path: 'list', name: 'admin.carriers.list', component: AdminCarriersList},
                    {path: 'new', name: 'admin.carriers.new', component: AdminCarriersNew},
                ]
            },
        ]
    },
]
