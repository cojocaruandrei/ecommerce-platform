
// state
export const state = {
    products: [],
    total_price: 0
}

// getters
export const getters = {
    products: state => state.products,
    totalPrice: state => state.total_price
}

// mutations
export const mutations = {
    set_db_cart(state, { cart }) {
        state.products = cart;
    },

    set_total_cart (state, { cart }) {
        let total = 0;
        cart.forEach(function(item) {
            let quantity = parseInt(item.quantity);
            total += parseFloat(item.price) * quantity;
        });
        state.total_price = total.toFixed(2);
    },

    add_to_cart(state, {product, quantity})
    {
        const record = state.products.find(p => p.id === product.id);

        if (!record) {
            state.products.push({
                ...product,
                quantity:  parseInt(quantity)
            });
        }else{
            record.quantity += parseInt(quantity);
        }
    },

    update_quantity(state, {product}){
        const record = state.products.find(p => p.id === product.id);
        record.quantity = parseInt(product.quantity);
    },

    remove_from_cart(state, {product, quantity}){
        const recordIndex = state.products.findIndex(p => p.id === product.id);
        if(recordIndex !== -1){
            state.products.splice(recordIndex, 1);
        }
    }
}

// actions
export const actions = {
    setDBCart({ commit }, cart){
        commit("set_db_cart",{
            cart: cart
        })
    },

    setTotalCart({ commit }, cart){
        commit("set_total_cart",{
            cart: cart
        })
    },

    addToCart({commit}, {product, quantity})
    {
        commit("add_to_cart", {
            product: product,
            quantity: quantity
        })
    },

    updateQuantity({commit}, {product}){
        commit("update_quantity", {
            product: product
        })
    },

    removeFromCart({commit}, {product, quantity}){
        commit("remove_from_cart", {
            product: product,
            quantity: quantity
        })
    }
}
