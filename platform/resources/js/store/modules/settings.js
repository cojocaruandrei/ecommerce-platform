import axios from 'axios'
import Cookies from 'js-cookie'
import * as types from '../mutation-types'

// state
export const state = {
  localization: {
    currency: null
  },
}

// getters
export const getters = {
  localization: state => state.localization,
}

// mutations
export const mutations = {
  fetch_settings (state, { settings }) {
    state.localization = settings
  },
}

// actions
export const actions = {
  async fetchSettings ({ commit }) {
    try {
      const { data } =  await axios.get('/api/getSettings', {params:{name: 'localization', group: 'frontend_settings'}})

      commit("fetch_settings", { settings: data })
    } catch (e) {
      console.log(e)
    }
  },
}
