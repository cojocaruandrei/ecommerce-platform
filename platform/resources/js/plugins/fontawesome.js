import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// import { } from '@fortawesome/free-regular-svg-icons'

import {
  faUser, faLock, faSignOutAlt, faCog, faShoppingCart, faCogs, faTachometerAlt, faPenNib, faSave, faTrash, faCubes, faPlus, faArrowLeft,faEye,faAddressBook, faEdit, faTruck, faFolderOpen
} from '@fortawesome/free-solid-svg-icons'

import {
  faGithub, faFacebook
} from '@fortawesome/free-brands-svg-icons'

library.add(
  faUser, faLock, faSignOutAlt, faCog, faGithub, faShoppingCart, faCogs, faTachometerAlt, faPenNib, faSave, faTrash, faCubes, faPlus, faArrowLeft, faEye,faFacebook,faAddressBook, faEdit, faTruck, faFolderOpen
)

Vue.component('fa', FontAwesomeIcon)
