import Vue from 'vue'
import store from '~/store'
import axios from  'axios'
import swal from 'sweetalert';
import router from '~/router'
import i18n from '~/plugins/i18n'
import App from '~/components/App'

import '~/plugins'
import '~/components'

import BootstrapVue from 'bootstrap-vue'
import {ServerTable, ClientTable, Event} from 'vue-tables-2';

Vue.use(BootstrapVue);
Vue.use(ServerTable, {}, false);
Vue.use(ClientTable, {}, false);

Vue.config.productionTip = false;

window.axios = axios;
window.swal = swal;
window.$vue = Vue;

Vue.prototype.$settings = store.state.settings;

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
})
