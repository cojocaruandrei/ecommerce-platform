<?php


    namespace App\Ecomm\Helpers;


    class ServerTable
    {
        protected $model;
        protected $filters;


        public function __construct($model, array $filters)
        {
            $this->model = $model;
            $this->filters = $filters;
        }

        public function getData($withTrashed = false)
        {
            $filters = $this->filters;
            $model = $this->model->newQuery();
            if($withTrashed){
                $model = $model->withTrashed();
            }

            //Filter
            if (isset($filters['query']))
            {
                $filters_arr = json_decode($filters['query']);
                foreach ($filters_arr as $key => $filter)
                {
                    $model->where($key, 'like', '%'.$filter.'%');
                }
            }

            $orderByColumn = $filters['orderBy'] ?? 'id';
            $orderByDirection = isset($filters['ascending']) && $filters['ascending'] == 1 ? 'ASC' : 'DESC';

            $take = $filters['limit'] ?? 10;
            $page = $filters['page'] ?? 1;
            $offset = ($page - 1) * $take;

            $response = [
                'count'=> $model->count()
            ];

            $response['data'] = $model
                ->orderBy($orderByColumn,$orderByDirection)
                ->offset($offset)
                ->take($take)
                ->get();
            return $response;
        }
    }