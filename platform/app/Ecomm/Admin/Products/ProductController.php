<?php

    namespace App\Ecomm\Admin\Products;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class ProductController extends Controller
    {
        protected $productService;


        public function __construct(ProductService $productService)
        {
            $this->productService = $productService;
        }

        /**
         * Display a listing of the resource.
         *
         * @param Request $request
         * @return \Illuminate\Http\Response
         */
        public function getProducts(Request $request)
        {
            $response = $this->productService->getProducts($request->all());

            return response($response);
        }

        public function getFeaturedProducts(Request $request)
        {
            $response = $this->productService->getFeaturedProducts($request->all());

            return response($response);
        }

        public function getProduct(Request $request)
        {
            $response = $this->productService->getProduct($request->input('id'));

            return response()->json($response);
        }

        public function saveProduct(Request $request)
        {
            $this->validate($request, [
                'name' => 'required',
//                'id_manufacturer' => 'required',
//                'id_category_default' => 'required',
                'reference' => 'required',
                'unity' => 'required',
                'price' => 'required',
            ]);

            $response = $this->productService->saveProduct($request->all());

            return response($response);
        }
    }
