<?php

    namespace App\Ecomm\Admin\Products;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;


    class Product extends Model
    {
        use SoftDeletes;

        protected $fillable = ['name', 'reference', 'id_manufacturer', 'id_category_default', 'tax', 'is_virtual', 'out_of_stock', 'available_for_order', 'unity', 'price', 'quantity'];

        public function photos(){
            return $this->hasMany('App\Ecomm\Admin\File\File');
        }
    }
