<?php

    namespace App\Ecomm\Admin\Products;

    use App\Ecomm\Admin\File\File;
    use App\Ecomm\Helpers\ServerTable;
    use Illuminate\Support\Facades\Storage;


    class ProductService
    {
        protected $productModel;


        public function __construct(Product $productModel)
        {
            $this->productModel = $productModel;
        }

        public function saveProduct(array $product){
            if(empty($product['id_manufacturer'])){
                $product['id_manufacturer'] = 0;
            }

            if(!isset($product['id_category_default'])){
                $product['id_category_default'] = 0;
            }

            $product_model = $this->productModel->create($product);

            if(!empty($product['productImages'])){
                foreach ($product['productImages'] as $image){
                    $image_id = $image['id'];
                    $file = File::find($image_id);
                    $file->product_id = $product_model->id;
                    $file->save();
                }
            }

            if($product_model){
                return $product_model;
            }
        }

        public function getProducts(array $filters)
        {
            $products = new ServerTable($this->productModel->with('photos'), $filters);
            $productsData = $products->getData();

            return $productsData;
        }

        public function getFeaturedProducts()
        {

            $products = $this->productModel->with('photos')->orderBy('id', 'desc')->take(8)->get();
            return $products;
        }

        public function getProduct(int $id){
            $product = $this->productModel->with('photos')->find($id);
            if(!$product){
                return false;
            }
            return $product;
        }
    }