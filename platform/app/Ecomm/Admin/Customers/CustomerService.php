<?php

    namespace App\Ecomm\Admin\Customers;

    use App\Ecomm\Helpers\ServerTable;
    use App\User;

    class CustomerService
    {
        protected $userModel;


        public function __construct(User $userModel)
        {
            $this->userModel = $userModel;
        }

        public static function getCustomerById($id){
            $user = User::where('id', $id)->get();
            $user = $user->toArray();

            $user = reset($user);
            return $user;
        }

        public function getCustomers(array $filters)
        {
            $customers = new ServerTable($this->userModel, $filters);
            $customersData = $customers->getData();

            return $customersData;
        }
    }