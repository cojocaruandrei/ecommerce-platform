<?php

    namespace App\Ecomm\Admin\Customers;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CustomerController extends Controller
    {
        protected $customerService;


        public function __construct(CustomerService $customerService)
        {
            $this->customerService = $customerService;
        }

        /**
         * Display a listing of the resource.
         *
         * @param Request $request
         * @return \Illuminate\Http\Response
         */

        public function getCustomers(Request $request){
            $response = $this->customerService->getCustomers($request->all());

            return response($response);
        }
    }
