<?php

namespace App\Ecomm\Admin\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    protected $settingsService;

    public function __construct(SettingsService $settings)
    {
        $this->settingsService = $settings;
    }

    public function index(Request $request)
    {
        $response = $this->settingsService->filter($request->all());

        return response()->json($response);
    }

    public function store(Request $request)
    {
        $filters = $this->settingsService->store($request->all());

        return response()->json($filters);
    }

    public function show($data)
    {
        $filters = $this->settingsService->show($data);

        return response()->json($filters);
    }

    public function saveSettingsAdmin(Request $request)
    {
        $response = $this->settingsService->saveSettingsAdmin($request);

        return response()->json($response);
    }

    public function getSettings(Request $request){
        $response = $this->settingsService->show($request->all());
        if($response === 403){
            return abort(403);
        }
        return response()->json($response);
    }
}
