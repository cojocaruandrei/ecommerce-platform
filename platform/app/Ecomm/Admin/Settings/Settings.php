<?php

    namespace App\Ecomm\Admin\Settings;

    use Illuminate\Database\Eloquent\Model;
    use DB, Validator, Request, Response;

    class Settings extends Model {

        protected $table = 'settings';
        public $timestamps = true;

        protected $fillable = ['name','group','value','json'];

        public static function setItem( $setting )
        {
            $existing_item = self::where('name',$setting['name'])->where('group',$setting['group'])->first();

            $data = [
                'name'=>$setting['name'], // week number
                'group'=>$setting['group'], // time_settings
                'value'=>$setting['value'], // 1 > 52
                'json'=>$setting['json'] // 0
            ];

            if( !isset($existing_item) )
            {
                self::create($data);
            }
            else
            {
                //            $existing_item->increment('value');
                $existing_item->update($data);
            }
        }

        public static function getItem($name,$group="")
        {
            $item = self::where('name',$name);

            if(!empty($group)){
                $item = $item->where('group',$group);
            }

            $item = $item->first();

            if(!$item)
            {
                return false;
            }

            $r = $item->value;
            if( $item->json == 1 )
            {
                $r = json_decode($r);
            }
            return $r;
        }

        public static function updateItem($setting, $key, $replace_val = false){
            $existing_item = self::where('name',$setting['name'])->where('group',$setting['group'])->first();
            if(!isset($existing_item)){
                $setting['value'] = "{}";
                self::setItem($setting);
            }
            $values = (array)json_decode($existing_item->value);

            if(isset($values) && isset($values[$key])){
                if($replace_val){
                    $values[$key] = $replace_val;
                }else{
                    unset($values[$key]);
                }
            }else{
                if($replace_val)
                {
                    $values[$key] = $replace_val;
                }else{
                    return false;
                }
            }

            $values = json_encode($values);

            $data = [
                'name'=>$setting['name'], // week number
                'group'=>$setting['group'], // time_settings
                'value'=> $values,
                'json'=>$setting['json'] // 0
            ];

            $existing_item->update($data);
        }

        public static function getItemUpdate($name,$group="")
        {
            $item = self::where('name',$name)->where('group',$group)->first();

            if(!$item)
            {
                return false;
            }

            $r = $item->updated_at;
            if( $item->json == 1 ) $r = json_decode($r);
            return $r;
        }
    }