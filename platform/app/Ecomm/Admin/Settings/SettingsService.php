<?php

namespace App\Ecomm\Admin\Settings;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;


class SettingsService
{
    protected $settingsModel;

    public function __construct(Settings $settings)
    {
        $this->settingsModel = $settings;
    }

    public function all()
    {

    }

    public static function store($info)
    {
        $setting = [];
        $setting['value'] = json_encode($info['options']['value']);
        $setting['name'] =$info['options']['name'];
        $setting['group'] = $info['options']['group'];
        $setting['json'] = $info['options']['json'];

        return Settings::setItem($setting);
    }

    public static function show($data)
    {
        $allowed_user_group = 'frontend_settings';
        $user = Auth::user();
        if(!isset($user) || $user->admin != 1){
           if($data['group'] != $allowed_user_group){
               return 403;
           }
        }
        return Settings::getItem($data);
    }

    public function saveSettingsAdmin($settings){
        $all_settings = $settings->all();

        foreach ($all_settings as $key => $set){
            $setting = [];

            $setting['json'] = 1;
            $setting['name'] = 'localization';
            $setting['group'] = 'frontend_settings';

            Settings::updateItem($setting, $key, $set);
        }
    }
}