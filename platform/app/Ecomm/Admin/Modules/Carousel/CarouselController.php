<?php

namespace App\Ecomm\Admin\Modules\Carousel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarouselController extends Controller
{
    protected $carouselService;

    public function __construct(CarouselService $carouselService)
    {
        $this->carouselService = $carouselService;
    }

    public function getEnabledImages(Request $request)
    {
        $response = $this->carouselService->getEnabledImages($request->all());

        return response($response);
    }

    public function deleteImage(Request $request)
    {
        $response = $this->carouselService->deleteImage($request->all());

        return response($response);
    }

}
