<?php

namespace App\Ecomm\Admin\Modules\Carousel;

use App\Ecomm\Admin\File\File;
use App\Ecomm\Admin\Settings\Settings;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class CarouselService
{
    protected $carouselModel;

    public function __construct(Carousel $carouselModel)
    {
        $this->carouselModel = $carouselModel;
    }

    public function all()
    {

    }

    public static function getEnabledImages($data)
    {
        $images = Settings::getItem($data);
        $enabledImages = [];
        foreach ($images as $id => $image){
            if($image){
                $enabledImages[] = File::getFileById($id);
            }
        }

        return $enabledImages;
    }

    public function deleteImage(array $data){
        $file_id = $data['id'];
        $file = File::getFileById($file_id);

        $setting = [
            'group' => 'frontend_carousel',
            'name'  => 'paths',
            'json'  => 1
        ];

        $res = File::deleteFile($file['path']);
        if($res){
            Settings::updateItem($setting, $file_id, false);
        }else{
            return ['error' => true];
        }

        return  ['error' => false];

    }
}