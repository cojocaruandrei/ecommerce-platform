<?php

namespace App\Ecomm\Admin\Carriers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarriersController extends Controller
{
    protected $carriersService;

    public function __construct(CarriersService $carriersService)
    {
        $this->middleware('isAdmin')->only('store');
        $this->carriersService = $carriersService;
    }

    public function index(Request $request)
    {
        $response = $this->carriersService->getCarriers($request->all());

        return response()->json($response);
    }

    public function store(Request $request)
    {
        $filters = $this->carriersService->store($request->all());

        return response()->json($filters);
    }

    public function destroy($id)
    {
        $response = $this->carriersService->deleteCarrier($id);

        return response()->json($response);
    }

//    public function show($data)
//    {
//        $filters = $this->carriersService->show($data);
//
//        return response()->json($filters);
//    }
}
