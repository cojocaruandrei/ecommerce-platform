<?php

namespace App\Ecomm\Admin\Carriers;

use App\Ecomm\Helpers\ServerTable;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;


class CarriersService
{
    protected $carriersModel;

    public function __construct(Carrier $carrierModel)
    {
        $this->carriersModel = $carrierModel;
    }

    public function getCarriers(array $filters)
    {
        $trashed = false;
        if(isset($filters['trashed']) && $filters['trashed'] == "true"){
            $trashed = true;
        }
        $carriers = new ServerTable($this->carriersModel, $filters);
        $carriers = $carriers->getData($trashed);

        return $carriers;
    }

    public function store(array $carrier)
    {
        $address_model = $this->carriersModel::create($carrier);

        if($address_model){
            return $address_model;
        }
    }

    public static function show($data)
    {

    }

    public function deleteCarrier($carrier_id){
        $carrier = $this->carriersModel::withTrashed()->find($carrier_id);
        if($carrier->trashed()){
            $carrier->deleted_at = null;
            return $carrier->save();
        }else{
            return $carrier->delete();
        }
    }

}