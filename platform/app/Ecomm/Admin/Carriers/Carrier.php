<?php

    namespace App\Ecomm\Admin\Carriers;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Carrier extends Model {
        use SoftDeletes;

        public $timestamps = true;

        protected $table = 'carriers';

        protected $fillable = ['name','description','time','price'];
    }