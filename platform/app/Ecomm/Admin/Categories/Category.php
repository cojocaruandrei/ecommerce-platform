<?php

    namespace App\Ecomm\Admin\Categories;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;


    class Category extends Model
    {
        use SoftDeletes;
        protected $table = 'categories';
        protected $fillable = ['name', 'id_parent'];

        public function products(){
            return $this->hasMany('App\Ecomm\Admin\Products\Product');
        }
    }
