<?php

    namespace App\Ecomm\Admin\Categories;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CategoryController extends Controller
    {
        protected $categoryService;


        public function __construct(CategoryService $categoryService)
        {
            $this->middleware('isAdmin')->except('index');
            $this->categoryService = $categoryService;
        }

        public function index(Request $request)
        {
            $response = $this->categoryService->getCategories($request->all());

            return response()->json($response);
        }

        public function store(Request $request)
        {
            $filters = $this->categoryService->store($request->all());

            return response()->json($filters);
        }

        public function destroy($id)
        {
            $response = $this->categoryService->deleteCategory($id);

            return response()->json($response);
        }
    }
