<?php

    namespace App\Ecomm\Admin\Categories;

    use App\Ecomm\Helpers\ServerTable;
    use Illuminate\Support\Facades\Storage;


    class CategoryService
    {
        protected $categoryModel;


        public function __construct(Category $categoryModel)
        {
            $this->categoryModel = $categoryModel;
        }

        public function store(array $category){
            $category_model = $this->categoryModel::create($category);

            if($category_model){
                return $category_model;
            }
        }

        public function getCategories(array $filters)
        {
            $categories = new ServerTable($this->categoryModel, $filters);
            $categoriesData = $categories->getData();

            return $categoriesData;
        }

        public function getCategory(int $id){
//            $product = $this->productModel::with('photos')->find($id);
//            if(!$product){
//                return false;
//            }
//            return $product;
        }
    }