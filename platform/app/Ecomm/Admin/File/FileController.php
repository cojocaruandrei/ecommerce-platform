<?php

    namespace App\Ecomm\Admin\File;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class FileController extends Controller
    {
        protected $fileService;


        public function __construct(FileService $fileUploadService)
        {
            $this->fileService = $fileUploadService;
        }

        /**
         * Display a listing of the resource.
         *
         * @param Request $request
         * @return \Illuminate\Http\Response
         */
        public function saveFile(Request $request)
        {
            $response = $this->fileService->saveFile($request->all());

            return response($response);
        }

        public function getFiles(Request $request){
            $response = $this->fileService->getFiles($request->all());

            return response($response);
        }
    }
