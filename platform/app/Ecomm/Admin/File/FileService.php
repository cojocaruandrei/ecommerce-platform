<?php

    namespace App\Ecomm\Admin\File;

    use App\Ecomm\Admin\Settings\Carousel;
    use App\Ecomm\Admin\Settings\Settings;
    use Illuminate\Support\Facades\Storage;


    class FileService
    {
        protected $fileUploadModel;


        public function __construct(File $fileUploadModel)
        {
            $this->fileUploadModel = $fileUploadModel;
        }

        public static function getFileById($id){
            $file = File::where('id', $id)->get();
            $file = $file->toArray();

            $file = reset($file);
            return $file;
        }

        public function saveFile(array $data){

            $fileStore = Storage::put($data['folder'], $data['file']);

            if($fileStore)
            {
                $path = '/' . $fileStore;

                $this->fileUploadModel->path = $path;
                $this->fileUploadModel->file_type = $data['type'];
                $res = $this->fileUploadModel->save();

                if(!$res){
                    $data = [
                        'error' => true,
                        'data'  => false
                    ];
                }else{
                    $data = [
                        'error' => false,
                        'data'  => $this->fileUploadModel->toArray()
                    ];
                }
                return $data;
            }
            return false;
        }

        public function getFiles(array $data){
            $file_type = $data['file_type'] ?? '';
            $arr = [];
            if($file_type == ""){
                $arr = File::all()->get();
            }else{
                $files = File::where('file_type', $file_type)->get();
                foreach ($files as $file){
                    $arr[$file['id']] = $file;
                }
            }

            return $arr;
        }

        public static function deleteFile($filePath){
            $res = Storage::delete($filePath);
            if($res){
                File::where('path', $filePath)->delete();
                return $filePath;
            }

            return $res;
        }
    }