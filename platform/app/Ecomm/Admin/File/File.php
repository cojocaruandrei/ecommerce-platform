<?php

    namespace App\Ecomm\Admin\File;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;


    class File extends Model
    {
        use SoftDeletes;

        /**
         * @var array
         */
        //    protected $table = 'offer_templates';

//        protected $guarded = ['errors'];

        public static function getFileById($id){
            return FileService::getFileById($id);
        }

        public static function deleteFile($id){
            return FileService::deleteFile($id);
        }
    }
