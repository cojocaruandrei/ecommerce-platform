<?php

    namespace App\Ecomm\Client\Address;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class AddressController extends Controller
    {
        protected $addressService;


        public function __construct(AddressService $addressService)
        {
            $this->middleware('isAdmin')->only('index');
            $this->addressService = $addressService;
        }

        public function index(Request $request)
        {
            $response = $this->addressService->getAddresses($request->all());

            return response()->json($response);
        }

        public function show($id)
        {
            $response = $this->addressService->getAddress($id);

            return response()->json($response);
        }

        public function store(Request $request)
        {
            $response = $this->addressService->saveAddress($request->all());

            return response()->json($response);
        }

        public function update(Request $request)
        {
            $response = $this->addressService->updateAddress($request->all());

            return response()->json($response);
        }

        public function destroy($id)
        {
            $response = $this->addressService->deleteAddress($id);

            return response()->json($response);
        }
    }
