<?php

    namespace App\Ecomm\Client\Address;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;


    class Address extends Model
    {
        use SoftDeletes;

        protected $table = 'addresses';

        protected $fillable = ['firstname', 'lastname', 'address1', 'address2', 'company', 'country', 'city', 'state', 'zip', 'phone', 'alias', 'id_customer'];

        public function customer()
        {
            return $this->belongsTo('App\Ecomm\Client\Address\Address', 'id');
        }
    }
