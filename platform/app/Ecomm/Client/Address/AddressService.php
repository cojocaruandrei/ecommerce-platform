<?php

    namespace App\Ecomm\Client\Address;

    use App\Ecomm\Helpers\ServerTable;

    class AddressService
    {
        protected $addressModel;


        public function __construct(Address $addressModel)
        {
            $this->addressModel = $addressModel;
        }

        public function getAddress($address_id){
            $address = Address::where('id', $address_id)->first();
            return $address;
        }

        public function getAddresses(array $filters){
            $addresses = new ServerTable($this->addressModel, $filters);
            $addresses = $addresses->getData();

            return $addresses;
        }

        public function saveAddress(array $address){
            $address['country'] = 'country';
            $address['state'] = 'state';

            $address_model = $this->addressModel::create($address);

            if($address_model){
                return $address_model;
            }
        }

        public function updateAddress(array $data){
            $new_model = Address::find($data['id']);
            $updated = $new_model->update($data);
            return $updated;
        }

        public function deleteAddress($address_id){
            $deleted = $this->addressModel::find($address_id)->delete();

            return $deleted;
        }
    }