<?php

    namespace App\Ecomm\Client\Cart;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CartController extends Controller
    {
        protected $cartService;


        public function __construct(CartService $cartService)
        {
            $this->cartService = $cartService;
        }

        public function index(Request $request)
        {
            $response = $this->cartService->getCart($request->all());

            return response()->json($response);
        }

        public function store(Request $request)
        {
            $response = $this->cartService->addToCart($request->all());

            return response()->json($response);
        }

        public function destroy($id)
        {
            $response = $this->cartService->deleteCart($id);

            return response()->json($response);
        }

        public function removeProductFromCart(Request $request)
        {
            $response = $this->cartService->removeProductFromCart($request->all());

            return response()->json($response);
        }
    }
