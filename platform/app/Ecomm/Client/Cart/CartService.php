<?php

    namespace App\Ecomm\Client\Cart;

    use App\Ecomm\Admin\File\File;
    use Illuminate\Support\Facades\Storage;


    class CartService
    {
        protected $cartModel;


        public function __construct(Cart $cartModel)
        {
            $this->cartModel = $cartModel;
        }

        public function addToCart(array $data)
        {
            $user = $data['user'];
            $cart = $data['cart'];

            $cartExists = Cart::where('id_customer', $user['id'])->first();

            if ($cartExists === null) {
                $dbCart = new Cart;
            }else{
                $dbCart = $cartExists;
            }


            $dbCart->id_customer = $user['id'];
            $dbCart->content = json_encode($cart);
            $res = $dbCart->save();

            return $res;
        }

        public function getCart(array $data){
            $user_id = $data['user_id'];
            $dbCart = [];

            $cartExists = Cart::where('id_customer', $user_id)->first();
            if($cartExists == null){
                return null;
            }else{
                $dbCart = json_decode($cartExists['content']);
                if(empty($dbCart)){
                    return $dbCart;
                }
            }

            return $dbCart;
        }

        public function removeProductFromCart(array $data){
            $user_id = $data['user_id'];
            $product_id = $data['product_id'];
            $found = false;

            $cartExists = Cart::where('id_customer', $user_id)->first();
            $dbCart = json_decode($cartExists['content']);

            foreach ($dbCart as $key => $product){
                if($product->id == $product_id){
                    $found = $key;
                    break;
                }
            }

            if($found !== false){
                unset($dbCart[$found]);
                array_values($dbCart);
                $cartExists['content'] = json_encode($dbCart);
                $cartExists->save();
                return true;
            }else{
                return false;
            }
        }
    }