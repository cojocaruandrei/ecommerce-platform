<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_customer');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('company')->nullable();
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('zip')->nullable();
            $table->string('phone');
            $table->string('alias');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
