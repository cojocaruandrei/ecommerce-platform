<?php

use Faker\Generator as Faker;

$factory->define(App\Ecomm\Admin\Products\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'reference' => $faker->unique()->swiftBicNumber,
        'id_manufacturer' => $faker->numberBetween(1, 10),
        'id_category_default' => $faker->numberBetween(1, 10),
        'tax' => 19,
        'is_virtual' => 0,
        'out_of_stock' => $faker->numberBetween(0, 1),
        'available_for_order' => 1,
        'unity' => '',
        'quantity' => $faker->numberBetween(10, 100),
        'price' => $faker->randomFloat(2,  0, 1200),
    ];
});
