<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    //Modules
    Route::get('getEnabledImages', 'Admin\Modules\Carousel\CarouselController@getEnabledImages')->name('getEnabledImages');

    //Frontend
    Route::prefix('product')->group(function () {
        Route::get('getProductImages', 'Admin\Products\ProductController@getProductImages')->name('getProductImages');
        Route::get('getProduct', 'Admin\Products\ProductController@getProduct')->name('getProduct');
    });

    Route::resource('address', 'Client\Address\AddressController');

//    Route::prefix('address')->group(function () {
//        Route::post('saveAddress', 'Client\Address\AddressController@saveAddress')->name('saveAddress');
//    });

    Route::get('getSettings', 'Admin\Settings\SettingsController@getSettings')->name('getSettings');
    Route::resource('carrier', 'Admin\Carriers\CarriersController');

    Route::resource('cart', 'Client\Cart\CartController');
    Route::post('remove-product', 'Client\Cart\CartController@removeProductFromCart');

    Route::resource('categories', 'Admin\Categories\CategoryController');

    Route::get('getProducts', 'Admin\Products\ProductController@getProducts')->name('getProducts');
    Route::get('getFeaturedProducts', 'Admin\Products\ProductController@getFeaturedProducts')->name('getFeaturedProducts');
    Route::get('getProducts', 'Admin\Products\ProductController@getProducts')->name('getProducts');


    Route::group(['middleware' => 'auth:api'], function () {
        Route::group(['middleware' => 'isAdmin'], function () {

            Route::post('upload', 'Admin\File\FileController@saveFile')->name('upload');
            Route::get('getFiles', 'Admin\File\FileController@getFiles')->name('getFiles');
    //        Route::delete('deleteFile', 'Admin\File\FileController@deleteFile')->name('deleteFile');

            Route::get('getCustomers', 'Admin\Customers\CustomerController@getCustomers')->name('getCustomers');

//            Route::get('getAddresses', 'Admin\Addresses\AddressController@getAddresses')->name('getAddresses');

            Route::post('saveProduct', 'Admin\Products\ProductController@saveProduct')->name('saveProduct');

            Route::post('setSettings', 'Admin\Settings\SettingsController@store')->name('setSettings');

            Route::post('saveSettingsAdmin', 'Admin\Settings\SettingsController@saveSettingsAdmin')->name('saveSettingsAdmin');
    //        Route::get('getSettingsAdmin', 'Admin\Settings\SettingsController@getSettingsAdmin')->name('getSettingsAdmin');

            //Modules
            Route::delete('deleteImage', 'Admin\Modules\Carousel\CarouselController@deleteImage')->name('deleteImage');
        });
    });

    Route::group(['middleware' => 'guest:api'], function () {

    });
